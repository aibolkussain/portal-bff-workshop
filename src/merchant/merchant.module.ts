import { Module, HttpModule } from '@nestjs/common';
import { MerchantController } from './merchant.controller';
import { MerchantService } from './merchant.service';

@Module({
  imports: [HttpModule],
  controllers: [MerchantController],
  providers: [MerchantService]
})
export class MerchantModule {}
