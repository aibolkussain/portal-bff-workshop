import { Injectable, HttpService } from '@nestjs/common';
import { EnvironmentService } from 'src/common/environment.service';

@Injectable()
export class MerchantService {
  merchantServiceBaseUrl: string;

  constructor(private readonly httpService: HttpService, private readonly envService: EnvironmentService) {
    this.merchantServiceBaseUrl = this.envService.get('MERCHANT_SERVICE_BASE_URL');
  }

  async getMerchantBySlug(slug: string) {
    const url = `${this.merchantServiceBaseUrl}/api/v1/merchants/by-slug/${slug}`;

    try {
      const { data: merchant } = await this.httpService.get(url).toPromise();

      return merchant;
    } catch(exception) {
      // log exception
      throw exception;
    }
  }
}
