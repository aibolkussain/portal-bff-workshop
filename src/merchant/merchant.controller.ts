import { Controller, Get, Query, Param } from '@nestjs/common';
import { MerchantService } from './merchant.service';
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { Merchant } from './models/merchant';

@ApiTags('merchant')
@Controller('merchant')
export class MerchantController {

  constructor(private readonly merchantService: MerchantService) {}

  @ApiResponse({ type: Merchant })
  @Get('by-slug/:browserUrl')
  findByBrowserUrl(@Query('locale') locale: string, @Param('browserUrl') merchantSlug: string) {
    try {
      const merchant: object = this.merchantService.getMerchantBySlug(merchantSlug);

      return merchant;
    } catch(err) {
      console.log(err);
    }
  }
}
