import { ApiProperty } from "@nestjs/swagger";

export class Merchant {
  @ApiProperty()
  id: string;

  @ApiProperty()
  slug: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  merchantNumber: number;
}