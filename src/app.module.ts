import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MerchantModule } from './merchant/merchant.module';
import { CommonModule } from './common/common.module';

@Module({
  imports: [MerchantModule, CommonModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
