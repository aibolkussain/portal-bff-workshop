import { Injectable } from '@nestjs/common';
import { cleanEnv, CleanEnv, port, url, str } from 'envalid';

interface Environment {
  NODE_ENV: string;
  PORT: number;
  MERCHANT_SERVICE_BASE_URL: string;
}

@Injectable()
export class EnvironmentService {
  private readonly envConfig: Environment;

  constructor() {
    this.envConfig = cleanEnv<{
      [K in Exclude<keyof Environment, keyof CleanEnv>]: Environment[K];
    }>(
      process.env,
      {
        PORT: port({ default: 3000 }),
        MERCHANT_SERVICE_BASE_URL: url(),
        NODE_ENV: str({
          devDefault: 'development',
          choices: ['development', 'test', 'production'],
        })
      }
    );
  }

  public get<K extends keyof Environment>(key: K): Environment[K] {
    return this.envConfig[key];
  }
}
