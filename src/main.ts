import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { CommonModule } from './common/common.module';
import { EnvironmentService } from './common/environment.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Portal BFF')
    .setDescription('Portal BFF documentation')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  const envService: EnvironmentService = app.select(CommonModule).get(EnvironmentService);

  const port = envService.get('PORT');
  
  await app.listen(port);
}
bootstrap();
